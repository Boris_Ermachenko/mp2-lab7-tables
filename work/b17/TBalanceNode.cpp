#include"TBalanceNode.h"

int TBalanceNode::GetBalance()const
{
	return this->Balance;
}

void TBalanceNode::SetBalance(int bal)
{
	if (bal != BalOk && bal != BalLeft && bal != BalRight)
	{
		throw "wrong value of ball";
	}
	else this->Balance = bal;
}

PTDatValue TBalanceNode::GetCopy()
{
	if (pValue != nullptr)
		return (PTDatValue)new TBalanceNode(this->Key, pValue->GetCopy(), nullptr, nullptr, Balance);
	else return (PTDatValue)new TBalanceNode(this->Key, nullptr, nullptr, nullptr, Balance);
}