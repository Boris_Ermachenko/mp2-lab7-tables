#include"TArrayTable.h"

TArrayTable::TArrayTable(int size)
{
	if (size <= 0 || size>TabMaxSize) throw "Wrong size";
	else
	{
		this->TabSize = size;
		this->CurrPos = 0;
		this->pRecs = new PTTabRecord[size];
		for (int i = 0; i < size; i++)
			this->pRecs[i] = nullptr;
	}
}

TArrayTable::~TArrayTable(){
	if (this == nullptr) throw "Error, nothing to delete";
	for (int i = 0; i < this->TabSize; i++)
	{
		if (this->pRecs[i] != nullptr)
		{
			if (this->pRecs[i]->pValue!=nullptr)
			delete this->pRecs[i]->pValue;
			delete this->pRecs[i];
		}
	}
	delete this->pRecs;
}

int TArrayTable::GetTabSize() const
{
	return this->TabSize;
}

int TArrayTable::GetCurrentPos() const
{
	return this->CurrPos;
}

TKey TArrayTable::GetKey() const
{
	return this->GetKey(CURRENT_POS);
}

PTDatValue TArrayTable::GetValuePTR() const
{
	return this->GetValuePTR(CURRENT_POS);
}

int TArrayTable::IsFull() const
{
	return (this->DataCount==this->TabSize);
}

TKey TArrayTable::GetKey(TDataPos mod) const
{
	if (mod){
		if (this->IsEmpty())
			throw"Empty Table";
	}
	else throw"No pos";
		
	int pos = 0;
	switch (mod)
	{
	case FIRST_POS:
		pos = 0;
		break;
	case CURRENT_POS:
		pos = this->CurrPos;
		break;
	case LAST_POS:
		pos = DataCount - 1;
	}
	if (pRecs[pos]->Key != "")
		return pRecs[pos]->Key; 
	else return nullptr;
}

PTDatValue TArrayTable::GetValuePTR(TDataPos mod) const
{
	if (this->IsEmpty())
		throw "Empty table";
	else
	{
		PTDatValue rez = nullptr;
		switch (mod)
		{
		case FIRST_POS:
			rez = this->pRecs[0];
			break;
		case CURRENT_POS:
			rez = this->pRecs[CurrPos];
			break;
		case LAST_POS:
			rez = this->pRecs[DataCount - 1];
			break;
		default:
			rez = nullptr;
			break;
		}

		return rez;
	}
}


int TArrayTable::Reset() 
{
	return this->CurrPos=0;
}

int TArrayTable::IsTabEnded() const
{
	return this->CurrPos==this->DataCount;
}

int TArrayTable::GoNext()
{
	if (this->IsTabEnded())
		throw"out of range";
	return this->CurrPos++;
}

int TArrayTable::SetCurrentPos(int pos)
{
	if (pos<0 || pos>DataCount)
		throw"Error, wrong pos";
	this->CurrPos=pos;
}

