#include"TArrayHash.h"

TArrayHash::TArrayHash(int Size, int Step) :THashTable()
{
	if (Size <= 0)
		throw "Wrong value of Size";
	pRecs = new PTTabRecord[Size];
	TabSize = Size;
	HashStep = Step;
	for (int i = 0; i < Size; i++)
	{
		pRecs[i] = nullptr;
	}
	pMark = new TTabRecord(string(""),nullptr);
}

TArrayHash::~TArrayHash()
{
	for (int i = 0; i < TabSize; i++)
		if (pRecs[i] != nullptr && pRecs[i] != pMark)
		{
			delete pRecs[i];
		}
	delete[] pRecs;
	delete pMark;
}

PTDatValue TArrayHash::FindRecord(TKey K)
{
	PTDatValue pResVal = nullptr;
	FreePos = -1;
	Efficiency = 0;
	CurrPos = HashFunc(K) % TabSize;
	for (int i = 0; i < TabSize; i++)
	{
		Efficiency++;
		if (pRecs[CurrPos] == nullptr) break;
		else if (pRecs[CurrPos] == pMark)
		{
			if (FreePos == -1)FreePos = CurrPos;
		}
		else
		if (pRecs[CurrPos]->Key == K)
		{
			pResVal = pRecs[CurrPos];
			break;
		}
			CurrPos = GetNextPos(CurrPos);
	}
	if (pResVal == nullptr) return nullptr;
	else return pResVal;
}

int TArrayHash::IsTabEnded() const
{
	return CurrPos==TabSize;
}

TKey TArrayHash::GetKey() const
{
	if (CurrPos<0 || CurrPos>TabSize)
		throw"wrong value of CurPos";
	else
	return pRecs[CurrPos]->Key;
}

PTDatValue TArrayHash::GetValuePTR() const
{
	if (CurrPos<0 || CurrPos>TabSize)
		throw"wrong value of CurPos";
	else if (pRecs[CurrPos]!=nullptr)
		return pRecs[CurrPos]->pValue;
	else throw "nothing to get back";
}

int TArrayHash::GoNext()
{

}
