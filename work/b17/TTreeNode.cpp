#include "TTreeNode.h"

TDatValue* TTreeNode::GetCopy()
{
	if (this->pValue != nullptr)
	{
		return (PTDatValue)(new TTreeNode(Key,pValue->GetCopy()));
	}
	else return (PTDatValue)(new TTreeNode(Key));
}

PTTreeNode TTreeNode::GetLeft() const
{
	return this->pLeft;
}

PTTreeNode TTreeNode::GetRight() const
{
	return this->pRight;
}