#include"TSortTable.h"

void TSortTable::SortData()
{
	if (this->IsEmpty())
		throw "nothing to sort";
	switch (SortMethod)
	{
	case INSERT_SORT:
		this->InsertSort(this->pRecs, this->DataCount);
		break;
	case MERGE_SORT:
		this->MergeSort(this->pRecs, this->DataCount);
		break;
	case QUIQ_SORT:
		this->QuiqSort(this->pRecs, this->DataCount);
		break;
	default:
		;
	}
}

void TSortTable::InsertSort(PTTabRecord *pMem, int DataCount)
{
	for (int i = 0; i < DataCount; i++)
	{
		int j=i;
		PTTabRecord tmp=pMem[i];
		while (j>0 && pMem[j]<pMem[j-1])
		{
			pMem[j] = pMem[j-1];
			j = j - 1;
		}
		pMem[j] = tmp;
	}
}

void TSortTable::QuiqSort(PTTabRecord *pMem, int DataCount)
{
	if (DataCount > 0){
		int  Privot = 0;
		QuiqSplit(pMem, DataCount, Privot);
		QuiqSort(pMem, Privot-1);
		QuiqSort(pMem + Privot, DataCount);
	}


}

void TSortTable::QuiqSplit(PTTabRecord *pMem, int Size, int &Privot)
{
	string Sup = this->pRecs[Size / 2]->GetKey() ;
	int i = 0;
	int j = Size;
	while (i<=j){
		if (pMem[i]->GetKey() > Sup && pMem[j]->GetKey() < Sup)
		{
			string tmp;
			tmp = pMem[i]->GetKey();
			pMem[i]->GetKey() = pMem[j]->GetKey();
			pMem[j]->GetKey() = tmp;
			i++;
			j--;
		}
		else
			if (pMem[i]->GetKey() < Sup)i++;
			else if (pMem[j]->GetKey() > Sup)j--;
	}
	Privot = i;
}

void TSortTable::MergeSort(PTTabRecord *pMem, int DataCount)
{
	PTTabRecord *pBuff=new PTTabRecord[DataCount];
	MergeSorter(pMem, pBuff, DataCount);
	delete[] pBuff;

}

void TSortTable::MergeSorter(PTTabRecord * &pData, PTTabRecord * &pBuff, int Size)
{
	MergeData(pData, pBuff, 0, Size);
}

void TSortTable::MergeData(PTTabRecord *&pData, PTTabRecord *&pBuff, int n1, int n2)
{
	int mid = (n1 + n2) / 2;
	if (n1 == n2) return;
	MergeData(pData, pBuff, n1, mid);
	MergeData(pData, pBuff, mid, n2);
	int i = n1;
	int j = mid+1;
	for (int step = 0; step < n2-n1+1; step++)
	{
		if (i > n2 || (pData[i] > pData[j] && j <=mid))
		{
			pBuff[step] = pData[j];
			j++;
		}
		else
		{
			pBuff[step] = pData[i];
			i++;
		}

	}

	for (int step = 0; step < n2 - n1 + 1; step++)
		pData[step + n1] = pBuff[step];
	
}

TSortTable::TSortTable(const TScanTable &tab) :TSortTable(tab.TabSize)
{
	this->DataCount = tab.DataCount;
	for (int i = 0; i < DataCount; i++){
		pRecs[i] = (PTTabRecord)tab.pRecs[i]->GetCopy();
	}
	SortMethod = MERGE_SORT;
	SortData();
	Reset();
}

void TSortTable::DelpRecs(PTTabRecord* pRecs, int len)
{
	for (int i = 0; i < len; i++)
	{
		if (pRecs[i] != nullptr)
			if (pRecs[i]->pValue != nullptr)
				delete pRecs[i]->pValue;
		delete pRecs[i];
	}
	delete[] pRecs;
}

TSortTable& TSortTable:: operator =(const TScanTable &tab)
{
	if (&tab != this)
	{
		int length = tab.TabSize;
		DelpRecs(pRecs, length);
		SortMethod = QUIQ_SORT;
		this->pRecs = new PTTabRecord[this->TabSize];
		this->TabSize = tab.TabSize;
		this->DataCount = tab.DataCount;
		for (int i = 0; i < this->TabSize; i++)
		{
			if (tab.pRecs[i] == nullptr)
				this->pRecs[i] = nullptr;
			else
			this->pRecs[i] = tab.pRecs[i];
		}
	}
	return *this;
}

TSortMethod TSortTable::GetSortMethod(void)
{
	return this->SortMethod;
}

void TSortTable::SetSortMethod(TSortMethod SortMethod)
{
	switch (SortMethod)
	{
	case QUIQ_SORT:this->SortMethod = QUIQ_SORT;
		break;
	case MERGE_SORT:this->SortMethod = MERGE_SORT;
		break;
	case INSERT_SORT: this->SortMethod = INSERT_SORT;
		break;
	default: throw"wrong SortMethod";
	}
}

PTDatValue TSortTable::FindRecord(TKey key)
{
	SortData();
	Reset();

	if (this->pRecs[0]->GetKey() > key || this->pRecs[this->DataCount]->GetKey() < key)
	{
		throw "Not found";
	}

	Efficiency = 0;
	int left = 0;
	int right = this->DataCount;
	while (left - right >= 0)
	{
		int midle = left + (right - left)/2;
		if (pRecs[midle]->GetKey() > key)
		{
			right = midle;
		}
		else if (pRecs[midle]->GetKey() < key)
			 {
				 left = midle;
			 }
		else if (pRecs[midle]->GetKey() == key)
			 {
				 this->CurrPos = midle;
				 return pRecs[midle]->GetValuePTR();
			 }
	}


}

void TSortTable::InsRecord(TKey key, PTDatValue pval)
{
	int flag = 0;
	SortData();
	Reset();
	if (this->IsFull())
	{
		throw "no place";
	}
	else
	{
		try
		{
			this->FindRecord(key);
			
		}
		catch (...){
			flag = 1;
			this->pRecs[this->DataCount]->SetValuePtr(pval);
			this->DataCount++;
			SortData();
			Reset();
		}
	}
	if (flag == 0)throw"already exist";
}


void TSortTable::DelRecord(TKey key)
{
	try
	{
		this->FindRecord(key);

	}
	catch (...){
		throw "No record with same key";
	}
	for (int i = this->CurrPos; i <DataCount-1; i++)
	{
		pRecs[i] = pRecs[i + 1];
	}
	DataCount--;
	if(pRecs[DataCount]!=nullptr)
		if (pRecs[DataCount]->pValue!=nullptr)
			delete pRecs[DataCount]->pValue;
	delete pRecs[this->DataCount];
	pRecs[this->DataCount] = nullptr;
}

