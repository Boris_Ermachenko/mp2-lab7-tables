#include"TTabRecord.h"


TTabRecord::TTabRecord(TKey key, PTDatValue pVal) :TDatValue()
{
	if (&key == nullptr)
		throw("Error, empty Key");
	else
	{
		this->Key = key;
		this->pValue = pVal;
	}
}
void TTabRecord::SetKey(TKey k)
{

	this->Key = k;
}

TKey TTabRecord::GetKey()
{
	return this->Key;
}

void TTabRecord::SetValuePtr(PTDatValue p)
{
	this->pValue = p;
}

PTDatValue TTabRecord::GetValuePTR()
{
	return this->pValue;
}

PTDatValue TTabRecord::GetCopy()
{
	if (this->pValue != nullptr)
	{
		PTTabRecord tmp = new TTabRecord(Key, pValue->GetCopy());
		PTDatValue t = (PTTabRecord)tmp;
		return tmp;
	}
	else
	{
		return (PTTabRecord)new TTabRecord(Key, nullptr);
	}
}

TTabRecord& TTabRecord::operator= (TTabRecord &tr)
{
	if (&tr != this)
	{
		if (this->pValue!=nullptr)
		{
			delete pValue;
		}
		this->Key = tr.Key;

		if (tr.pValue == nullptr)
			this->pValue = nullptr;
		delete this->pValue;/*��� �����?*/
		this->pValue = pValue->GetCopy();
	}
		return *this;
}

int TTabRecord::operator==(const TTabRecord &tr)
{
	return ( this->Key==tr.Key);
}

int TTabRecord::operator<(const TTabRecord &tr)
{
	return (this->Key < Key);
}

int TTabRecord:: operator>(const TTabRecord &tr)
{
	return (this->Key> Key);
}