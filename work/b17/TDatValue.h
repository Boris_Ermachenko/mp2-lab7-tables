#ifndef TDATVALUE_H
#define TDATVALUE_H

#include<iostream>


class TDatValue;
typedef TDatValue *PTDatValue;

class TDatValue
{ 
public:
	virtual TDatValue* GetCopy() = 0;
	~TDatValue() {}

};

#endif