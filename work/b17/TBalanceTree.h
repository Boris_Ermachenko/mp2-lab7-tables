#include"TTreeTable.h"
#include"TBalanceNode.h"

class  TBalanceTree : public TTreeTable  {
protected:
	int InsBalanceTree(PTBalanceNode &pNode, TKey k, PTDatValue pVal);
	int DelBalanceTree(PTBalanceNode pNode, TKey k, PTBalanceNode pParent);
	int LeftTreeBalancing(PTBalanceNode &pNode); // ������. ������ ���������
	int RightTreeBalancing(PTBalanceNode &pNode);// ������. ������� ���������
public:
	TBalanceTree() :TTreeTable(){} // �����������
	//�������� ������
	virtual void InsRecord(TKey k, PTDatValue pVal); // ��������
	virtual void DelRecord(TKey k);                   // �������
};