#include "TTreeTable.h"
#define MAXSIZETREE 1000000
void TTreeTable::DeleteTreeTab(PTTreeNode pNode)
{
	if (pNode->GetLeft() != nullptr)
	{
		DeleteTreeTab(pNode->GetLeft());
	}

	if (pNode->GetRight() != nullptr)
	{
		DeleteTreeTab(pNode->GetRight());
	}
	if (pNode != nullptr){
		if (pNode->GetValuePTR() != nullptr)
			delete pNode->pValue;
		delete pNode;
		pNode = nullptr;
	}
}

int TTreeTable::IsFull() const
{
	if (this->DataCount > MAXSIZETREE)
		return false;
	else return true;
}

PTDatValue TTreeTable::FindRecord(TKey key)
{
	PTTreeNode cur = pRoot;
	ppRef = nullptr;
	this->Efficiency=0;
	while (cur!=nullptr)
	{
		Efficiency++;
		if (cur->GetKey() == key)
		{
			ppRef = cur;
			return cur->GetValuePTR();
		}
		else if (cur->GetKey() > key)
		{
			//ppRef = cur;
			cur = cur->GetLeft();
		}
		else
		{
			//ppRef = cur;
			cur = cur->GetRight();
		}
	}
	throw "not found";
}

void TTreeTable::InsRecord(TKey key, PTDatValue pVal)
{
	if (IsFull())
	{
		throw"table is full";
	}
	PTTreeNode cur = pRoot;
	PTTreeNode rezult = nullptr;
	ppRef = nullptr;
	this->Efficiency = 0;
	while (cur != nullptr)
	{
		Efficiency++;
		if (cur->GetKey() == key)
		{
			rezult = cur;
		}
		else if (cur->GetKey() > key)
		{
			ppRef = cur;
			cur = cur->GetLeft();
		}
		else
		{
			ppRef = cur;
			cur = cur->GetRight();
		}
	}
	if (rezult != nullptr)
	{
		if (ppRef->GetKey() < key)
			ppRef->pLeft = new TTreeNode(key, pVal);
		else
			ppRef->pRight = new TTreeNode(key, pVal);
	}
	if (pRoot == nullptr)
	{
		pRoot = new TTreeNode(key, pVal);
	}
	DataCount++;
}

void TTreeTable::DelRecord(TKey key)
{
	try
	{
		FindRecord(key);
	}
	catch (...)
	{
		throw"not found to del";
	}
	
	if (ppRef->GetRight() == nullptr && ppRef->GetLeft() == nullptr)
	{
		if (ppRef != nullptr)
		{
			if (ppRef->GetValuePTR() != nullptr)
				delete ppRef->GetValuePTR();
			delete ppRef;
			ppRef = nullptr;
		}
	}

	if (ppRef->GetLeft() != nullptr && ppRef->GetRight()==nullptr)
	{
		PTTreeNode tmp = ppRef->GetLeft();

			if (ppRef->GetValuePTR() != nullptr)delete ppRef->pValue;
			delete ppRef;
		
		ppRef = tmp;
	}

	if (ppRef->GetRight() != nullptr)
	{

		PTTreeNode tmpParent = ppRef->GetRight();
		PTTreeNode tmpChild_left = ppRef->GetRight();
		while (tmpChild_left->GetLeft() != nullptr)
		{
			tmpParent = tmpChild_left;
			tmpChild_left = tmpChild_left->GetLeft();
		}
		if (tmpChild_left->GetRight() != nullptr)
		{
			tmpParent->pLeft = tmpChild_left->GetRight();
		}
		if (ppRef->GetValuePTR() != nullptr)
			delete ppRef->pValue;

		ppRef->pValue = tmpChild_left->GetValuePTR();
		tmpChild_left->pValue = nullptr;
		ppRef->Key = tmpChild_left->GetKey();
		delete tmpChild_left;

		if (tmpParent->GetValuePTR() != nullptr)
		{
			delete tmpParent->pValue;
		}
		delete tmpParent;

	}

}

TKey TTreeTable::GetKey() const
{
	if (pCurrent == nullptr)
	{
		throw"Current ie empty";
	}
	else
	{
		return pCurrent->Key;
	}
}

PTDatValue TTreeTable::GetValuePTR() const
{
	if (pCurrent == nullptr)
	{
		throw"Current ie empty";
	}
	else
	{
		return pCurrent->pValue;
	}
}

int TTreeTable::Reset()
{
	if (pRoot == nullptr)
	{
		throw"empty";
	}
	else
	{
		PTTreeNode TmpCur = pCurrent = pRoot;
		while (!St.empty())St.pop();
		while (TmpCur->GetLeft()!=nullptr)
		{
			pCurrent = TmpCur;
			St.push(TmpCur);
			TmpCur = TmpCur->GetLeft();
		}
		return CurrPos = 0;
	}
}

int TTreeTable::IsTabEnded() const
{
	return DataCount == CurrPos;
}

int TTreeTable::GoNext()
{
	if (pCurrent->GetRight() == nullptr && !St.empty())
	{
		pCurrent = St.top();
		return CurrPos++;
	}
	PTTreeNode cur = pCurrent->GetRight();
	while (cur->GetLeft() != nullptr)
	{
		St.push(cur);
		pCurrent = cur;
		cur = cur->GetLeft();
	}
	pCurrent = cur;
	return CurrPos++;
}