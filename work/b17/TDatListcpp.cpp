#include"TDatList.h"
#include <iostream>
using namespace std;

TDatList::TDatList()
{
	pFirst = pLast = pCurrLink = pPrevLink = pStop = nullptr;
	ListLen = 0;
	CurrPos = 0;
}

PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink)
{
	return new TDatLink(pVal, pLink);
	//PTDatLink tmp = new TDatLink(pVal,pLink);
	//if (tmp == nullptr){
	//	cout << "Memory error";
	//}
	//return tmp;
}
void TDatList::DelLink(PTDatLink pLink)
{
	if (pLink != nullptr)
	{
		if (pLink->pValue != nullptr)
			delete pLink->pValue;
		delete pLink;
	}

}
PTDatValue TDatList::GetDatValue(TLinkPos mode) const
{
	PTDatLink tmp = nullptr;
	switch (mode)
	{
	case FIRST:
		tmp = pFirst;
		break;
	case CURRENT:
		tmp = pCurrLink;
		break;
	case LAST:
		tmp = pLast;
		break;
	default:

		break;
	}
	if (tmp != nullptr)
	{
		return tmp->GetDatValue();
	}
	else return nullptr;

}

int TDatList::SetCurrentPos(int pos)         // ���������� ������� �����
{
	if (pos <= GetListLength() && pos >= 0){
		Reset();
		for (int i = 0; i < pos; i++)
		{
			GoNext();
		}
		return 1;
	}
	else
	{
		throw "Length_ERROR";
		cout << "Length_ERROR";
		return 0;
	}

}

int TDatList::GetCurrentPos(void) const
{
	return CurrPos;
}

void TDatList::Reset(void){
	pPrevLink = pStop;
	if (IsEmpty()){
		CurrPos = -1;
		pCurrLink = nullptr;
	}
	else
	{
		CurrPos = 0;
		pCurrLink = pFirst;
	}
}
int TDatList::IsListEnded(void) const
{
	return pCurrLink == pStop;
}

int TDatList::GoNext(void)
{
	if (!IsListEnded()){
		pPrevLink = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		CurrPos++;
		return 1;
	}
	else
		return 0;
}

void TDatList::InsFirst(PTDatValue pVal)
{

	PTDatLink tmp = GetLink(pVal, pFirst);
	if (tmp){
		tmp->SetNextLink(pFirst);
		pFirst = tmp;
		if (ListLen != 0){
			if (CurrPos == 0){
				pCurrLink = tmp;
			}
			else CurrPos++;
		}
		else{
			pLast = tmp;
			Reset();
		}
		ListLen++;
	}
	else
		cout << "ERROR_Memory";
	/*
	if (tmp)
	{
	if (GetListLength() == 0)
	{
	pCurrLink = pFirst = pLast = tmp;
	CurrPos = -1;
	}
	tmp->SetNextLink(pFirst);
	ListLen++;
	CurrPos++;
	pFirst = tmp;
	if (CurrPos == 1)
	pPrevLink = tmp;
	}*/
}

void TDatList::InsLast(PTDatValue pVal)
{/*

 if (IsEmpty())
 {
 pFirst = pLast = pCurrLink = pPrevLink = new TDatLink();
 pLast->SetDatValue(pVal);
 pLast->SetNextLink(pStop);
 }
 else
 {
 pLast->SetNextLink(new TDatLink(pVal, pStop));
 pLast = pLast->GetNextDatLink();
 }
 ListLen++;*/
	PTDatLink tmp = GetLink(pVal, pStop);
	if (tmp)
	{
		if (ListLen == 0)
		{
			InsFirst(pVal);
		}
		else
		{
			pLast->SetNextLink(tmp);
			pLast = tmp;
			ListLen++;
			//if (IsListEnded())
			//	pCurrLink = tmp;
		}
	}
}
void TDatList::InsCurrent(PTDatValue pVal)
{
	if (pCurrLink == pFirst || IsEmpty())
		InsFirst(pVal);
	else
		//if (pCurrLink == pLast);
		//InsLast(pVal);
		//else
	{
		PTDatLink tmp = GetLink(pVal, pCurrLink);
		if (tmp){
			tmp->SetNextLink(pCurrLink);
			pPrevLink->SetNextLink(tmp);
			ListLen++;
			pCurrLink = tmp;
		}
		else
			cout << "ERROR_Memory";
	}
}

void TDatList::DelFirst(void)
{

	if (IsEmpty())
		cout << "Nothing to del";
	else{
		if (pFirst == pLast)
		{
			pLast = pStop;
			DelLink(pFirst);
			pFirst = pStop;
			ListLen--;
			Reset();
		}
		else
		{
			PTDatLink tmp = new TDatLink();
			tmp = pFirst->GetNextDatLink();
			DelLink(pFirst);
			ListLen--;
			pFirst = tmp;
			if (CurrPos == 0)
				pCurrLink = pFirst;
			else if (CurrPos == 1)
			{
				pPrevLink = pStop;
			}

			if (CurrPos > 0) CurrPos--;

		}

	}
}

void TDatList::DelCurrent(void)
{
	if (IsEmpty())
		cout << "Nothing to Del";
	else
	{
		if (pFirst == pCurrLink)
			DelFirst();
		else if (pCurrLink == pLast)
		{
			DelLink(pLast);
			pLast = pPrevLink;
			pLast->SetNextLink(pStop);
			pCurrLink = pStop;
			ListLen--;
		}
		else {
			PTDatLink tmp = pCurrLink;
			pPrevLink->SetNextLink(pCurrLink->GetNextLink());
			pCurrLink = pCurrLink->GetNextDatLink();
			DelLink(tmp);

		}

	}

}
void TDatList::DelList(void){
	Reset();
	//for (int i = 0; i < ListLen; i++)
	//{
	//	DelCurrent();//DelLink(pCurrLink);
	//}
	CurrPos = -1;//*/
	//pFirst = pLast = pPrevLink  = pCurrLink = pStop;
	while (!IsEmpty())
		DelFirst();
	CurrPos = -1;//*/
}