# Отчет по Лабораторной работе №6: Тексты

## Цели и задачи

В рамках лабораторной работы ставится задача разработки учебного редактора текстов с поддержкой следующих операций:

-	выбор текста для редактирования (или создание нового текста);
-	демонстрация текста на экране дисплея;
-	поддержка средств указания элементов (уровней) текста;
-	вставка, удаление и замена строк текста;
-	запись подготовленного текста в файл.

## Описание работы

В качестве структуры хранения текстов используются иерархические списки.Звенья такого списка содержат по два указателя: один используется для организации отношения следования по элементам текста одного и того же уровня, а другой – для указания отношения следования по иерархии уровней представления текста (от более высокого уровня к ниже расположенному).
Структура звеньев:
-	каждое звено структуры хранения содержит два поля указателей и поле для хранения данных;
-	нижний уровень иерархической структуры звеньев ограничивается уровнем строк, а не символов. Это повышает эффективность использования памяти, так как в случае отдельного уровня для хранения символов, затраты на хранение служебной информации (указатели) в несколько раз превышают затраты на хранение самих данных.При использовании строк в качестве атомарных элементов поле для хранения данных является массивом символов заданного размера;
-	количество уровней и количество звеньев на каждом уровне может быть произвольным;
-	при использовании звена на атомарном уровне поле указателя на нижний уровень структуры устанавливается равным значению NULL;
-	при использовании звена на структурных уровнях представления текста поле для хранения строки текста используется для размещения наименования соответствующего элемента текста;
-	количество используемых уровней представления в разных фрагментах текста может быть различным.

**Модульная структура программы:**
* TTextLink.h, TTextLink.cpp – модуль с классом для звена текста;
* TText.h, TText.cpp – модуль с классом, реализующим операции над текстом;


##Код программы

**TTextLink.h**

```

#define _CRT_SECURE_NO_WARNINGS

#ifndef TTEXTLINK_H
#define TTEXTLINK_H

#define TextLineLength 40
#define MemSize 40
#include"TDatValue.h"
#include <cstring>
#include <iostream>

using namespace std;

class TTextLink;
class TText;
class TTextMem;

typedef TTextLink *PTTextLink;
typedef char TStr[TextLineLength];
class TTextMem {
protected:
	PTTextLink pFirst;     // указатель на первое звено
	PTTextLink pLast;      // указатель на последнее звено
	PTTextLink pFree;      // указатель на первое свободное звено
	friend class TTextLink;
};
typedef TTextMem *PTTextMem;

class TTextLink : public TDatValue {
protected:
	TStr Str;  // поле для хранения строки текста
	PTTextLink pNext, pDown;  // указатели по тек. уровень и на подуровень
	static TTextMem MemHeader; // система управления памятью
public:
	static void InitMemSystem(int size = MemSize); // инициализация памяти
	static void PrintFreeLink(void);  // печать свободных звеньев
	void * operator new (size_t size); // выделение звена
	void operator delete (void *pM);   // освобождение звена
	static void MemCleaner(TText &txt); // сборка мусора
	TTextLink(TStr s = nullptr, PTTextLink pn = nullptr, PTTextLink pd = nullptr);
	/*{
		pNext = pn; 
		pDown = pd;
		if (s != nullptr) {
			strcpy_s(Str, s);
			cout << "";
		}
		else 
			Str[0] = '\0';

	}*/
	~TTextLink() {}
	int IsAtom() { return pDown == nullptr; } // проверка атомарности звена
	PTTextLink GetNext() { return pNext; }
	PTTextLink GetDown() { return pDown; }
	PTDatValue GetCopy() { return new TTextLink(Str, pNext, pDown); }
protected:
	virtual void Print(ostream &os) { os << Str; }
	friend class TText;
};

#endif 

```

**TTextLink.cpp**

```
#include"TTextLink.h"
#include "TText.h"

TTextMem TTextLink::MemHeader;

TTextLink::TTextLink(TStr s, PTTextLink pn, PTTextLink pd)
{
	pNext = pn;
	pDown = pd;
	if (s != nullptr)
		 strcpy_s(Str, s);
	else
		Str[0] = '\0';
	
}
void TTextLink::InitMemSystem(int size)
{
	char *tmp=new char[sizeof(TTextLink)*size];
	MemHeader.pFirst = (PTTextLink)tmp;
	MemHeader.pFree = (PTTextLink)tmp;
	MemHeader.pLast = (PTTextLink)tmp[size - 1];

	PTTextLink LinkTmp = MemHeader.pFirst;
	for (int i = 0; i < size-1; i++)
	{
		LinkTmp->pNext = LinkTmp + 1;
		LinkTmp++;
	}
	LinkTmp->pNext=nullptr;
}

void TTextLink::PrintFreeLink()
{

	PTTextLink LinkTmp = MemHeader.pFree;
	while (LinkTmp != nullptr)
	{
		cout << LinkTmp->Str<<endl;
		LinkTmp = LinkTmp->pNext;
	}
}

void * TTextLink::operator new(size_t size)
{
	PTTextLink pLink = MemHeader.pFree;
	if (MemHeader.pFree!=nullptr)
	{
		MemHeader.pFree = pLink->pNext;
	}
	return pLink;
}

void TTextLink:: operator delete(void *pM)
{
	PTTextLink pLink = (PTTextLink)pM;
	pLink->pNext = MemHeader.pFree;
	MemHeader.pFree = pLink;
}

void TTextLink::MemCleaner(TText & txt)
{
		
	txt.Reset();
	while (!txt.IsTextEnded())
	{
		txt.SetLine("&&&" + txt.GetLine());
		txt.GoNext();
	}

	PTTextLink pLink = MemHeader.pFree;
	while (pLink != MemHeader.pLast)
	{
		strcpy(pLink->Str, "&&&");
		pLink++;

	}

	pLink=MemHeader.pFirst;
	while (pLink != MemHeader.pLast)
	{

		if (pLink->Str == "&&&") 
			strcpy(pLink->Str, pLink->Str + 3);
		else 
			delete(pLink);
		pLink++;

	}
}
```

**TText.h**
```
#ifndef TTEXT_H
#define TTEXT_H

#include"TDataCom.h"
#include"TTextLink.h"
#include<string>
#include<stdio.h>
#include<iostream>
#include<fstream>
#include <stack>
class TText;


using namespace std;

typedef TText *PTText;

class TText : public TDataCom {
protected:
	PTTextLink pFirst;      // указатель корня дерева
	PTTextLink pCurrent;      // указатель текущей строки
	stack< PTTextLink > Path; // стек траектории движения по тексту
	stack< PTTextLink > St;   // стек для итератора
	PTTextLink GetFirstAtom(PTTextLink pl); // поиск первого атома
	void PrintText(PTTextLink ptl);         // печать текста со звена ptl
	PTTextLink ReadText(ifstream &TxtFile); //чтение текста из файла
public:
	TText(PTTextLink pl = nullptr);
	~TText() { pFirst = nullptr; }
	PTText getCopy();
	// навигация
	int GoFirstLink(void); // переход к первой строке
	int GoDownLink(void);  // переход к следующей строке по Down
	int GoNextLink(void);  // переход к следующей строке по Next
	int GoPrevLink(void);  // переход к предыдущей позиции в тексте
	// доступ
	string GetLine(void);   // чтение текущей строки
	void SetLine(string s); // замена текущей строки 
	// модификация
	void InsDownLine(string s);    // вставка строки в подуровень
	void InsDownSection(string s); // вставка раздела в подуровень
	void InsNextLine(string s);    // вставка строки в том же уровне
	void InsNextSection(string s); // вставка раздела в том же уровне
	void DelDownLine(void);        // удаление строки в подуровне
	void DelDownSection(void);     // удаление раздела в подуровне
	void DelNextLine(void);        // удаление строки в том же уровне
	void DelNextSection(void);     // удаление раздела в том же уровне
	// итератор
	int Reset(void);              // установить на первую звапись
	int IsTextEnded(void) const;  // текст завершен?
	int GoNext(void);             // переход к следующей записи
	//работа с файлами
	void Read(char * pFileName);  // ввод текста из файла
	void Write(char * pFileName); // вывод текста в файл
	//печать
	void Print(void);             // печать текста

	friend TTextLink;
	int TextLevel = 0;
};

#endif
```

**TText.cpp**

```
#include"TText.h"
#include"TTextLink.h"

#define MAX_LENGTH 40
PTTextLink TText::GetFirstAtom(PTTextLink pl)
{
	PTTextLink tmp = pl;
	while (!tmp->IsAtom())
	{
		St.push(tmp);
		tmp = tmp->GetNext();
	}
	return tmp;
}

void TText::PrintText(PTTextLink ptl)
{
	for (int i = 0; i < TextLevel; i++)
	{
		cout << "-";
	}
	cout << ">" << ptl->Str;

	TextLevel++;
	PrintText(ptl->GetDown());
	TextLevel--;
	PrintText(ptl->GetNext());
	cout << "%s",ptl->Str;
}


PTTextLink TText::ReadText(ifstream &TxtFile)
{
	char strTmp[255];
	PTTextLink pLink=new TTextLink();
	PTTextLink pTmpLink=pLink;
	while (!TxtFile.eof())
	{
		TxtFile.getline(strTmp,255,'\n');
		//PTTextLink pLink = new TTextLink();
		if (strTmp[0] == '}')
		{
			break;
		}
		else
			if (strTmp[0] == '{')
			{
				pLink->pDown=ReadText(TxtFile);
			}
			else
			{
				pLink->pNext = new TTextLink(strTmp,nullptr,nullptr);//strcpy(pLink->Str, &strTmp[0]);
				pLink = pLink->pNext;
			}
	}
	pLink=pTmpLink;
	if (pLink->pDown == nullptr)
	{
		pTmpLink = pTmpLink->pNext;
		delete pLink;
	}
	return pTmpLink;
}


TText::TText(PTTextLink pl)
{
	if (pl == nullptr)
	{
		pl = new TTextLink();
		cout << "g";
	}
	pFirst = pl;
}

PTText TText::getCopy()
{
	cout << "0";
	PTTextLink p1;
	PTTextLink p2;
	cout << "+";
	PTTextLink pLink = pFirst;
	cout << "0";
	PTTextLink cp1 = nullptr;
	cout << "*";
	if (pFirst == nullptr) { cout << "nothing to copy"; throw "nothing to Copy";  return this; }
	while (!St.empty()){
		cout << "-";
		St.pop();
	}
	while (1)
		{

			cout << "/1";
			if (pLink != nullptr) {
				St.push(pLink);
				pLink = pLink->pDown;
				cout << "2";
			}
			else
				if (St.empty()){
					cout << "gg";
					break;
				}
				else
					{
						cout << "g&g";
						p1 = St.top();
						St.pop();
						if (strstr("Copy", p1->Str) == nullptr)
						{
							p2 = new TTextLink("Copy",p1,cp1);
							St.push(p2);
							pLink = p1->GetNext();
							cp1 = nullptr;
							cout << "3";

						}
						else
						{
							strncpy(p1->Str,p1->GetNext()->Str,MAX_LENGTH);
							p1->pNext = cp1;
							cp1 = p1;
							cout << "4";
						}
					}
		}

	
	return new TText(cp1);

};

int TText::GoFirstLink()
{
	while (!Path.empty())
	{
		Path.pop();
	}
	if ((pCurrent = pFirst) == nullptr)
		return 0;
	else return 1;
}


int TText::GoDownLink()
{
	if (pCurrent != nullptr && pCurrent->GetDown()!= nullptr)
	{
		Path.push(pCurrent);
		pCurrent = pCurrent->GetDown();
		return 1;
	}
	else
	{
		throw "Error"; return 0;
	}
}

int TText::GoNextLink()
{
	if (pCurrent != nullptr && pCurrent->GetNext() != nullptr)
	{
		Path.push(pCurrent);
		pCurrent = pCurrent->GetNext();
		return 1;
	}
	else { throw "Error";  return 0; }
}

int TText::GoPrevLink()
{

	if (!Path.empty())
	{
		pCurrent = Path.top();
		Path.pop();
		return 1;
	} 
	throw "Error";
	return 0;
}

string TText::GetLine()
{
	if (pCurrent == nullptr)
	{
		throw "Error";
		return "";
	}
	else 
		return string(pCurrent->Str);
}

void TText::SetLine(string s)
{
	if (pCurrent == nullptr)
		throw "Error";
	else 
		if(s.length()>MAX_LENGTH)
		throw"too long";
		else strncpy(pCurrent->Str,s.c_str(),MAX_LENGTH);
		pCurrent->Str[MAX_LENGTH - 1] = '\0';
}

void TText::InsDownLine(string s)
{
	if (pCurrent == nullptr)
		throw"Error";
	else if (s.length() > MAX_LENGTH)
		throw "Error, too long";
	else
	{
		PTTextLink pd = pCurrent->pDown;
		PTTextLink pl = new TTextLink("",pd,nullptr);

		strncpy_s(pl->Str, s.c_str(), MAX_LENGTH);
		pl->Str[MAX_LENGTH - 1] = '\0';
		pCurrent->pDown = pl;
	}
}

void TText::InsDownSection(string s)
{
	if (pCurrent == nullptr)
		throw"Error";
	else if (s.length() > MAX_LENGTH)
		throw "Error, too long";
	else
	{
		PTTextLink pd = pCurrent->pDown;
		PTTextLink pl = new TTextLink("", nullptr, pd);

		strncpy(pl->Str, s.c_str(), MAX_LENGTH);
		pl->Str[MAX_LENGTH - 1] = '\0';
		pCurrent->pDown = pl;
	}
}

void TText::InsNextLine(string s)
{
	if (pCurrent == nullptr)
		throw"Error";
	else if (s.length() > MAX_LENGTH)
		throw "Error, too long";
		else
		{
			PTTextLink pn = pCurrent->GetNext();
			PTTextLink pl = new TTextLink("",pn,nullptr);

			strncpy(pl->Str,s.c_str(),MAX_LENGTH);
			pl->Str[MAX_LENGTH - 1] = '\0';
			pCurrent->pNext = pl;
		}
}

void TText::InsNextSection(string s)
{
	if (pCurrent == nullptr)
		throw"Error";
	else if (s.length() > MAX_LENGTH)
		throw "Error, too long";
	else
	{
		PTTextLink pn = pCurrent->GetNext();
		PTTextLink pl = new TTextLink("", nullptr, pn);

		strncpy(pl->Str, s.c_str(), MAX_LENGTH);
		pl->Str[MAX_LENGTH - 1] = '\0';
		pCurrent->pNext = pl;
	}
}

void TText::DelDownLine(void)
{
	if  (pCurrent == nullptr || pCurrent->GetDown()==nullptr)
		throw"Error";
	else
	{
		PTTextLink tmp= pCurrent->pDown->GetNext();

		if (pCurrent->pDown->pDown == nullptr)
			pCurrent->pDown = tmp;
	}
}

void TText::DelDownSection(void)
{
	if (pCurrent == nullptr || pCurrent->GetDown() == nullptr)
		throw"Error";
	else
	{
		PTTextLink tmp = pCurrent->pDown->GetNext();

		pCurrent->pDown = tmp;
	}
}


void TText::DelNextLine(void)
{
	if (pCurrent == nullptr || pCurrent->GetNext() == nullptr)
		throw"Error";
	else
	{
		PTTextLink tmp = pCurrent->pNext->GetNext();

		if (pCurrent->pNext->pDown == nullptr)
			pCurrent->pNext = tmp;
	}
}

void TText::DelNextSection(void)
{
	if (pCurrent == nullptr || pCurrent->GetNext() == nullptr)
		throw"Error";
	else
	{
		PTTextLink tmp = pCurrent->pNext->GetNext();


			pCurrent->pNext = tmp;
	}
}

int TText::Reset()
{
	PTTextLink pl = pFirst;
	PTTextLink pc;
	while (!Path.empty())
	{
		Path.pop();
	}
	pCurrent = pFirst;
	if (pCurrent != nullptr)
	{
		St.push(pCurrent);
		if (pCurrent->GetNext() != nullptr)
			St.push(pCurrent->GetNext());
		if (pCurrent->GetDown() != nullptr)
			St.push(pCurrent->GetDown());
	}
	return 1;
}

int TText::IsTextEnded() const 
{
	return St.empty();
}

int TText::GoNext()
{
	if (!IsTextEnded()){
		pCurrent = St.top();
		St.pop();
		if (pCurrent != pFirst){
			
			if (pCurrent->GetNext() != nullptr)
				St.push(pCurrent->GetNext());
			if (pCurrent->GetDown() != nullptr)
				St.push(pCurrent->GetDown());
		}
	}
	return IsTextEnded();
}

void TText::Read(char * pFileName)
{
	TextLevel = 0;
	ifstream fread;
	fread.open(pFileName);
	if (fread.is_open())
	{
		pFirst = ReadText(fread);
	}
	else
	{
		throw"Cannot open file";
	}
}

void TText::Write(char * pFileName)
{
	ofstream fwrite;
	int lel = 0;
	int count = 0;
	int arr[20] = {};
	fwrite.open(pFileName);
	if (fwrite.is_open())
	{
		fwrite << "{";
		PTTextLink ptl = pFirst;
		while (!St.empty())
		{
			St.pop();
		}
		while (1)
		{
			if (ptl != nullptr)
			{
				St.push(ptl);
				fwrite << ptl->Str<<"\n";
				ptl = ptl->GetDown();
				if (ptl != nullptr)
				{
					arr[count++]=lel++;
					for (int i = 0; i < arr[count]; i++)
						fwrite << "_";
					fwrite << "{" << "\n";
				}
			}
			else
				if (St.empty())
					break;
				else

				{
					ptl = St.top();
					St.pop();
					ptl = ptl->GetNext();
					if (ptl == nullptr)
					{
						count--;
						arr[count]--;
						for (int i = 0; i < arr[count]; i++)
							fwrite << "_";
						fwrite << "}"<<"\n";
					}
				}
		}

	}
	else
	{
		throw"Cannot open file";
	}
}

void TText::Print(void)
{
	PrintText(pFirst);
}
```



##Тесты для проверки правильности работы программ
```
#include gtestgtest.h
#include TText.h
#include tdatacom.h
#include iostream

#include gtestgtest.h
#include TText.h

TEST(TTextLink, can_initMem_default)
{
	ASSERT_NO_THROW(TTextLinkInitMemSystem());
}
TEST(TTextLink, can_create_link_default)
{
	TTextLinkInitMemSystem();
	PTTextLink pLink;
	ASSERT_NO_THROW(pLink = new TTextLink);
}
TEST(TTextLink, can_create_link_with_text)
{
	TTextLinkInitMemSystem();
	PTTextLink pLink;
	ASSERT_NO_THROW(pLink = new TTextLink(Text));
}
TEST(TTextLink, can_create_link)
{
	TTextLinkInitMemSystem();
	PTTextLink pLink;
	PTTextLink pLink2 = new TTextLink;
	PTTextLink pLink3 = new TTextLink;

	ASSERT_NO_THROW(pLink = new TTextLink(Text, pLink2, pLink3));
	}
TEST(TTextLink, can_get_next_link)
{
	TTextLinkInitMemSystem();
	PTTextLink pLinkDown = new TTextLink;
	PTTextLink pLinkNext = new TTextLink;
	
	PTTextLink pLink = new TTextLink(Text, pLinkNext, pLinkDown);

	EXPECT_EQ(pLink-GetNext(), pLinkNext);
}
TEST(TTextLink, can_get_down_link)
{
	TTextLinkInitMemSystem();
	PTTextLink pLinkN = new TTextLink;
	PTTextLink pLinkD = new TTextLink;
	PTTextLink pLink = new TTextLink(Text, pLinkN, pLinkD);
	
	EXPECT_EQ(pLink-GetDown(), pLinkD);
}
TEST(TTextLink, can_get_copy_link)
{
	TTextLinkInitMemSystem();
	PTTextLink pLinkN = new TTextLink;
	PTTextLink pLinkD = new TTextLink;
	PTTextLink pLink = new TTextLink(Text, pLinkN, pLinkD);
	PTTextLink pLinkCopy = (PTTextLink)pLink-GetCopy();
	bool flag = true;
	
	if (pLink-GetDown() != pLinkCopy-GetDown()  pLink-GetNext() != pLinkCopy-GetNext())
	flag = false;

	EXPECT_EQ(true, flag);
	}
TEST(TTextLink, can_delete_link)
{
	TTextLinkInitMemSystem();
	PTTextLink pLink = new TTextLink;
	ASSERT_NO_THROW(delete pLink);
}
TEST(TText, can_initMem)
{
	ASSERT_NO_THROW(TTextLinkInitMemSystem(2));
	
	PTTextLink pLink1, pLink2, pLink3;
	pLink1 = new TTextLink;
	pLink2 = new TTextLink;
	pLink3 = new TTextLink;
	
	EXPECT_NE(nullptr, pLink2);
	EXPECT_EQ(nullptr, pLink3);
	}
TEST(TText, can_create_link_with_text)
{
	ASSERT_NO_THROW(TTextLink(111));
}
TEST(TText, can_create_null_link)
{
	ASSERT_NO_THROW(TTextLink());
}
TEST(TText, can_create_link)
{
	PTTextLink pNext = new TTextLink();
	PTTextLink pDown = new TTextLink();
	ASSERT_NO_THROW(TTextLink(qwer, pNext, pDown));

}
TEST(TText, can_create)
{
	PTTextLink plink;

	ASSERT_NO_THROW(TText(plink));
}
TEST(TText, can_get_copy)
{
	TTextLinkInitMemSystem();
	PTTextLink tmp = new TTextLink(text);
	
	TText txt(tmp);
	TText txt2;

	txt2 = txt.getCopy();

	txt.Reset();

	txt2-Reset();

	EXPECT_EQ(txt.GetLine(), txt2-GetLine());
}
TEST(TText, try_ins_down_line_if_current_null)
{
	TTextLinkInitMemSystem();
	TText txt(new TTextLink());
	ASSERT_ANY_THROW(txt.InsDownLine(text));

}
TEST(TText, try_ins_down_section_if_current_null)
{
	TTextLinkInitMemSystem();
	TText txt(new TTextLink());
	ASSERT_ANY_THROW(txt.InsDownSection(text));
	}
TEST(TText, try_ins_next_line_if_current_null)
{
	TTextLinkInitMemSystem();
	TText txt(new TTextLink());
	
	ASSERT_ANY_THROW(txt.InsNextLine(text));
	}
TEST(TText, try_ins_next_section_if_current_null)
 {
	 TTextLinkInitMemSystem();
	TText txt(new TTextLink());
	
	
	ASSERT_ANY_THROW(txt.InsNextSection(text));
}
TEST(TText, can_ins_down_line)
{
	TTextLinkInitMemSystem(2);
	TText txt(new TTextLink(text));
	
	txt.GoFirstLink();
	txt.InsDownLine(text2);
	txt.GoDownLink();

	EXPECT_EQ(txt.GetLine(), text2);
	}
TEST(TText, try_ins_down_long_line)
{
	TTextLinkInitMemSystem(2);
	TText txt(new TTextLink(text));
	
		txt.GoFirstLink();
		ASSERT_ANY_THROW(txt.InsDownLine(opopopopopopopopoptttttttttttttttttttttttttttttttttttttt));

	
}
TEST(TText, can_ins_down_section)
 {
	TTextLinkInitMemSystem(2);
	TText txt(new TTextLink(text));
	txt.GoFirstLink();
	txt.InsDownSection(text2);
	txt.GoDownLink();
	EXPECT_EQ(txt.GetLine(), text2);
}
TEST(TText, try_ins_down_long_section)
{
	TTextLinkInitMemSystem(2);
	TText txt(new TTextLink(text));
	txt.GoFirstLink();

	ASSERT_ANY_THROW(txt.InsDownSection(opopopopopopopopoptttttttttttttttttttttttttttttttttttttt));
}
TEST(TText, can_ins_next_line)
{
	TTextLinkInitMemSystem(2);
	TText txt(new TTextLink(text));

	txt.GoFirstLink();
	txt.InsNextLine(text2);
	txt.GoNextLink();
	EXPECT_EQ(txt.GetLine(), text2);
}
TEST(TText, try_ins_next_long_line)
{
	TTextLinkInitMemSystem(2);
	TText txt(new TTextLink(text));

	txt.GoFirstLink();
	
	
	ASSERT_ANY_THROW(txt.InsNextLine(opopopopopopopopoptttttttttttttttttttttttttttttttttttttt););
}

TEST(TText, can_ins_next_section)
{
	TTextLinkInitMemSystem(2);
	TText txt(new TTextLink(text));
	
	txt.GoFirstLink();
	txt.InsNextSection(text2);
	txt.GoNextLink();

	EXPECT_EQ(txt.GetLine(), text2);
}

TEST(TText, try_ins_next_long_section)
{
	TTextLinkInitMemSystem(2);
	TText txt(new TTextLink(text));

	txt.GoFirstLink();
	
	ASSERT_ANY_THROW(txt.InsNextSection(opopopopopopopopoptttttttttttttttttttttttttttttttttttttt));
}

TEST(TText, no_del_down_line_which_have_down_line)
 {
	TTextLinkInitMemSystem(3);
	TText txt(new TTextLink(text));
	
	txt.GoFirstLink();
	txt.InsDownLine(text2);
	txt.GoDownLink();
	txt.InsDownLine(text3);
	txt.GoFirstLink();
	txt.DelDownLine();
	txt.GoDownLink();
	EXPECT_EQ(txt.GetLine(), text2);
}
TEST(TText, can_del_down_line)
{
	TTextLinkInitMemSystem(2);
	TText txt(new TTextLink(text));
	txt.GoFirstLink();
	txt.InsDownLine(text2);
	txt.DelDownLine();

	ASSERT_ANY_THROW(txt.GoDownLink());
}
TEST(TText, no_del_next_line_which_have_down_line)
{
	TTextLinkInitMemSystem(3);
	TText txt(new TTextLink(text));
	txt.GoFirstLink();
	txt.InsNextLine(text2);
	txt.GoNextLink();
	txt.InsDownLine(text3);
	txt.GoFirstLink();
	txt.DelNextLine();
	txt.GoNextLink();

	EXPECT_EQ(txt.GetLine(), text2);
}
TEST(TText, can_del_next_line)
{
	TTextLinkInitMemSystem(2);
	TText txt(new TTextLink(text));

	txt.GoFirstLink();
	txt.InsNextLine(text2);
	txt.DelNextLine();
	ASSERT_ANY_THROW(txt.GoNextLink(););
}
TEST(TText, can_del_down_section)
{
	TTextLinkInitMemSystem(3);
	TText txt(new TTextLink(text));
	txt.GoFirstLink();
	txt.InsDownSection(text2);
	txt.GoDownLink();
	txt.InsNextLine(text3);
	txt.GoFirstLink();
	txt.DelDownSection();
	txt.GoDownLink();
	EXPECT_EQ(txt.GetLine(), text3);
}
TEST(TText, can_del_next_section)
{
	TTextLinkInitMemSystem(3);
	TText txt(new TTextLink(text));
	
	txt.GoFirstLink();
	txt.InsNextSection(text2);
	txt.GoNextLink();
	txt.InsNextLine(text3);
	txt.GoFirstLink();
	txt.DelNextSection();
	txt.GoNextLink();
	EXPECT_EQ(txt.GetLine(), text3);
}
TEST(TText, can_reset)
{
	TTextLinkInitMemSystem(4);
	TText txt(new TTextLink(text1));
	
	txt.GoFirstLink();
	txt.InsNextLine(text2);
	txt.GoNextLink();
	txt.InsNextLine(text3);
	txt.GoNextLink();
	txt.InsNextLine(text4);
	txt.GoNextLink();
	txt.Reset();
	EXPECT_EQ(txt.GetLine(), text1);
}
TEST(TText, can_go_next)
{
	TTextLinkInitMemSystem(2);
	TText txt(new TTextLink(text));
	txt.GoFirstLink();
	txt.InsNextLine(text2);
	txt.GoNextLink();

	txt.Reset();
	txt.GoNext();
	EXPECT_EQ(txt.GetLine(), text2);
}
TEST(TText, can_go_next_if_end)
{
	TTextLinkInitMemSystem(3);
	TText txt(new TTextLink(text));
	txt.GoFirstLink();
	txt.InsNextLine(text2);
	txt.GoNextLink();
	txt.InsNextLine(text3);
	txt.GoNextLink();
	txt.GoNext();
	txt.GoNext();
	EXPECT_EQ(txt.GetLine(), text3);
}
TEST(TText, can_chek_end_text_return_false)
{
	TTextLinkInitMemSystem(4);
	TText txt(new TTextLink(text));
	txt.GoFirstLink();
	txt.InsNextLine(text2);
	txt.GoNextLink();
	txt.InsNextLine(text3);
	txt.GoNextLink();
	txt.InsNextLine(text4);
	txt.GoNextLink();
	txt.Reset();
	txt.GoNext();

EXPECT_EQ(txt.IsTextEnded(), 0);
}
TEST(TText, can_chek_end_text_return_true)
{
	TTextLinkInitMemSystem(4);
	TText txt(new TTextLink(text));
	txt.GoFirstLink();
	txt.InsNextLine(text2);
	txt.GoNextLink();
	txt.InsNextLine(text3);
	txt.GoNextLink();
	txt.InsNextLine(text4);
	txt.GoNextLink();
	txt.Reset();
	txt.GoNext();
	txt.GoNext();
	txt.GoNext();
	txt.GoNext();
	EXPECT_EQ(txt.IsTextEnded(), 1);
}
TEST(TText, can_go_first_link)
{
	TTextLinkInitMemSystem(2);
	TText txt(new TTextLink(text));
	txt.GoFirstLink();
	EXPECT_EQ(txt.GetLine(), text);
}
TEST(TText, try_go_down_link_if_down_null)
{
	TTextLinkInitMemSystem(2);
	TText txt(new TTextLink(text));
	ASSERT_ANY_THROW(txt.GoDownLink());
}
TEST(TText, can_go_down_link)
{
	TTextLinkInitMemSystem(3);
	TText txt(new TTextLink(text));
	txt.GoFirstLink();
	txt.InsDownSection(text2);
	txt.GoDownLink();
	EXPECT_EQ(txt.GetLine(), text2);
}
TEST(TText, try_go_next_link_if_next_null)
{
	TTextLinkInitMemSystem(3);
	TText txt(new TTextLink(text));	
	ASSERT_ANY_THROW(txt.GoNextLink());
}
TEST(TText, can_go_next_link)
{
	TTextLinkInitMemSystem(3);
	TText txt(new TTextLink(text));
	txt.GoFirstLink();
	txt.InsNextSection(text2);
	txt.GoNextLink();
	EXPECT_EQ(txt.GetLine(), text2);
	}
TEST(TText, try_go_prev_link_if_not_prev)
{
	TTextLinkInitMemSystem(3);
	TText txt(new TTextLink(text));
	
	txt.GoFirstLink();
	
	ASSERT_ANY_THROW(txt.GoPrevLink());
}
TEST(TText, can_go_prev_link)
{
	TTextLinkInitMemSystem(2);
	TText txt(new TTextLink(text));
	txt.GoFirstLink();
	txt.InsDownSection(text2);
	txt.GoDownLink();
	txt.GoPrevLink();
	string s = txt.GetLine();
	EXPECT_EQ(s, text);
}
TEST(TText, can_get_line)
{
	TTextLinkInitMemSystem(3);
	TText txt(new TTextLink(text));
	txt.GoFirstLink();
	EXPECT_EQ(txt.GetLine(), text);
}
TEST(TText, can_get_line_if_current_null)
{
	TTextLinkInitMemSystem(3);
	TText txt(new TTextLink());
	txt.GoFirstLink();
	EXPECT_EQ(txt.GetLine(), );
}
TEST(TText, can_set_line)
{
	TTextLinkInitMemSystem(3);
	TText txt(new TTextLink(text));
	txt.GoFirstLink();
	txt.SetLine(text2);
	EXPECT_EQ(txt.GetLine(), text2);
}
TEST(TText, try_set_long_line)
{
	TTextLinkInitMemSystem();
	TText txt(new TTextLink(text));
	txt.GoFirstLink();
	ASSERT_ANY_THROW(txt.SetLine(opopopopopopopopoptttttttttttttttttttttttttttttttttttttt));
}
TEST(TText, txt_set_line_if_current_null)
{
	TTextLinkInitMemSystem();
	TText txt;
	ASSERT_ANY_THROW(txt.SetLine(text));

}
TEST(TText, can_get_copy_text)
{
	TTextLinkInitMemSystem(12);
	TText txt(new TTextLink(text));
	PTText txtCopy;
	bool flag = true;
	txt.GoFirstLink();
	txt.InsNextLine(text2);
	txt.GoNextLink();
	txt.InsNextLine(text3);
	txt.GoNextLink();
	txt.InsNextLine(text4);
	txt.GoNextLink();
	txt.InsNextLine(text5);
	txt.GoNextLink();
	txt.InsNextLine(text6);
	txt.GoNextLink();
	txtCopy = txt.getCopy();
	for (txt.Reset(), txtCopy-Reset(); txt.IsTextEnded()  txtCopy-IsTextEnded(); txt.GoNext(), txtCopy-GoNext())
	{
		if (txt.GetLine() != txtCopy-GetLine())
		flag = false;
	}
		EXPECT_TRUE(flag);
}
TEST(TText, if_change_copy_don_not_change_original)
{
	TTextLinkInitMemSystem(12);
	TText txt(new TTextLink(text));
	PTText txtCopy;
	bool flag = true;
	txt.GoFirstLink();
	txt.InsNextLine(text2);
	txt.GoNextLink();
	txt.InsNextLine(text3);
	txt.GoNextLink();
	txt.InsNextLine(text4);
	txt.GoNextLink();
	txt.InsNextLine(text5);
	txt.GoNextLink();
	txt.InsNextLine(text6);
	txt.GoNextLink();
	txtCopy = txt.getCopy();
	txt.GoFirstLink();
	txt.SetLine(CHANGE);
	txtCopy-GoFirstLink();
	EXPECT_NE(txtCopy-GetLine(), CHANGE);
}
TEST(TText, if_change_copy_don_not_change_originalss)
{
	TTextLinkInitMemSystem(10);
	TText txt(new TTextLink(text));
	PTText txtCopy;
	bool flag = true;
	txt.GoFirstLink();
	txt.InsNextLine(text2);
	txt.GoNextLink();
	txt.InsNextLine(text3);
	txt.GoNextLink();
	txt.InsNextLine(text4);
	txt.GoNextLink();
	txt.InsNextLine(text5);
	txt.GoNextLink();
	txtCopy = txt.getCopy();
	txt.GoFirstLink();
	txt.SetLine(CHANGE);
	txtCopy-GoFirstLink();
	EXPECT_NE(txtCopy-GetLine(), CHANGE);
}

```


##Выводы
Мне удалось выполнить поставленную задачу, понять и реализовать алгоритмы для работы с иерархическими списками, 
познакомиться с новым для себя способом работы с динамической памятью 
