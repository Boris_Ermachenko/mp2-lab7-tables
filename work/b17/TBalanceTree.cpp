#include"TBalanceTree.h"

int TBalanceTree::InsBalanceTree(PTBalanceNode &pNode, TKey key, PTDatValue pValue)
{
	if (pNode == nullptr)
	{
		pNode = new TBalanceNode(key, pValue);
		DataCount++;
		return 1;
	}
	if (pNode->Key < key)
	{
		if (InsBalanceTree((PTBalanceNode &)pNode->pRight, key, pValue))
		{
			if(RightTreeBalancing(pNode))
				return 1;
		}
	}
	if (pNode->Key > key)
	{
		if (InsBalanceTree((PTBalanceNode &)pNode->pLeft, key, pValue))
		{
			if (LeftTreeBalancing(pNode))
				return 1;
		}
	}
	if (pNode->Key == key)
	{
		throw "record already exist";
	}

}

int TBalanceTree::LeftTreeBalancing(PTBalanceNode &pNode)
{
	if (pNode->Balance == BalOk){
		pNode->SetBalance(BalLeft);
		return 1;
	}
	else if (pNode->Balance == BalRight){
		pNode->SetBalance(BalOk);
		return 0;
	}
	else if (pNode->Balance == BalLeft)
	{
		PTBalanceNode pLeft = (PTBalanceNode &)pNode->pLeft;
		if (pLeft->Balance == BalLeft)
		{
			pNode->pLeft = pLeft->pRight;
			pLeft->pRight = pNode;
			pNode->SetBalance(BalOk);
			pNode = pLeft;
			return 0;
		}
		else
		{
			PTBalanceNode pNextRight = (PTBalanceNode &)pLeft->pRight;
			pLeft->pRight = pNextRight->pLeft;
			pNextRight->pLeft = pLeft;
			pNode->pLeft = pNextRight->pRight;
			pNextRight->pRight = pNode;

			if (pNextRight->Balance == BalLeft)
			{
				pLeft->SetBalance(BalRight);
			}
			else pLeft->SetBalance(BalOk);

			if (pNextRight->Balance == BalRight)
			{
				pNode->SetBalance(BalLeft);
			}
			else pNode->SetBalance(BalOk);

			pNode = pNextRight;
			pNode->SetBalance(BalOk);
			return 0;
		}
	}
	else{
		throw"error value";
	}
}

int TBalanceTree::RightTreeBalancing(PTBalanceNode &pNode)
{
		if (pNode->Balance == BalOk){
		pNode->SetBalance(BalLeft);
		return 1;
	}
		else if (pNode->Balance == BalLeft){
		pNode->SetBalance(BalOk);
		return 0;
	}
	else if (pNode->Balance == BalRight)
	{
		PTBalanceNode pRight = (PTBalanceNode &)pNode->pRight;
		if (pRight->Balance == BalRight){
			pNode->pRight = pRight->pLeft;
			pRight->pLeft = pNode;
			pNode->SetBalance(BalOk);
			pNode = pRight;
			return 0;
		}
		else
		{
			PTBalanceNode pNextLeft = (PTBalanceNode &)pRight->pLeft;
			pRight->pLeft = pNextLeft->pRight;
			pNextLeft->pRight = pRight;
			pNode->pRight = pNextLeft->pLeft;
			pNextLeft->pLeft = pNode;

			if (pNextLeft->Balance==BalLeft)
			{
				pNode->SetBalance(BalRight);
			}
			else pNode->SetBalance(BalOk);

			if (pNextLeft->Balance == BalRight)
			{
				pRight->SetBalance(BalLeft);
			}
			else pRight->SetBalance(BalOk);

			pNode = pNextLeft;
			pNode->SetBalance(BalOk);
			return 0;
		}
	}
	else{
		throw"error value";
	}
}

void TBalanceTree::InsRecord(TKey Key, PTDatValue pVal)
{
	if (this->IsFull())
		throw"table is full";
	else InsBalanceTree((PTBalanceNode&)pRoot,Key, pVal);
}

void TBalanceTree::DelRecord(TKey Key)
{
	if (this->IsEmpty())
	throw "nothing to delete";
	else DelBalanceTree((PTBalanceNode&)this->pRoot, Key, nullptr);
}

int TBalanceTree::DelBalanceTree(PTBalanceNode pNode, TKey k,PTBalanceNode pParent)
{
	if (pNode == nullptr)
	{
		throw"record with this Key does not exist";
	}
	else if (pNode->Key>k)
	{
		if(DelBalanceTree((PTBalanceNode)pNode->pLeft, k, pNode))
			RightTreeBalancing(pNode);
	}
	else if (pNode->Key < k)
	{
		if (DelBalanceTree((PTBalanceNode)pNode->pRight, k, pNode))
			LeftTreeBalancing(pNode);
	}
	else
	{
		if (pParent == nullptr)// esly eto pRoot - sluchae
		{
			if (pNode->pLeft != nullptr)
			{
				pRoot = pNode->pLeft;
				if (pNode->pRight != nullptr)
					pRoot->pRight = pNode->pRight;
				delete pNode->pValue;
				delete pNode;
				DataCount--;
				RightTreeBalancing((PTBalanceNode&) pRoot);
				return 0;

			}
			else if (pNode->pRight != nullptr)
			{
				pRoot = pNode->pRight;
				if (pNode->pLeft != nullptr)
					pRoot->pLeft = pNode->pLeft;
				delete pNode->pValue;
				delete pNode;
				DataCount--;
				LeftTreeBalancing((PTBalanceNode&)pRoot);
				return 0;
			}
			else //odin sovsem :(
			{
				delete pNode->pValue;
				delete pNode;
				pRoot = nullptr;
				DataCount--;
				return 0;
			}
		}
		else if (pNode->pLeft == nullptr && pNode->pRight == nullptr) //esly list
		{
			if (pParent->pLeft == pNode){
				delete pNode->pValue;
				delete pNode;
				pParent->pLeft = nullptr;

			}
			else
			{
				delete pNode->pValue;
				delete pNode;
				pParent->pRight = nullptr;

			}
			DataCount--;
			return 1;
		}
		else if (pNode->pLeft == nullptr)// esly net sleva
		{
			if (pParent->pLeft == pNode)
			{
				pParent->pLeft = pNode->pRight;
				delete pNode->pValue;
				delete pNode;
				DataCount--;
			}
			else
			{
				pParent->pRight = pNode->pRight;
				delete pNode->pValue;
				delete pNode;
				DataCount--;
			}
			return 1;
		}
		else  if (pNode->pRight == nullptr)//esly net sprava
		{
			if (pParent->pLeft == pNode)
			{
				pParent->pLeft = pNode->pLeft;
				delete pNode->pValue;
				delete pNode;
				DataCount--;
			}
			else
			{
				pParent->pRight = pNode->pLeft;
				delete pNode->pValue;
				delete pNode;
				DataCount--;
			}
			return 1;
		}
		else if (pNode->pLeft!=nullptr && pNode->pRight!=nullptr)// est i sleva, i sprava
		{
			PTBalanceNode max = (PTBalanceNode)pNode->pRight;
			while (max->pLeft != nullptr)
			{
				max = (PTBalanceNode)max->pLeft;
			}
			pNode->Key = max->Key;
			pNode->pValue = max->pValue;
			DelBalanceTree((PTBalanceNode)pNode->pRight, max->Key, pNode);//sprava mb ne poosto, togda tak
			max = nullptr;
		}
	}
}