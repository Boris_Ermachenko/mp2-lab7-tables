#include"TScanTable.h"

PTDatValue TScanTable::FindRecord(TKey k)
{
	if (this->IsEmpty())
		throw "Error, empty table";
	Efficiency = 0;
	for (int i = 0; i < this->DataCount; i++)
	{
		Efficiency++;
		if (this->pRecs[i]->GetKey() == k)
		{
			this->CurrPos = i;
			return pRecs[i]->GetValuePTR();
		}
	}
	return nullptr;
}

void TScanTable::InsRecord(TKey k, PTDatValue val)
{
	if (this->IsFull())
		throw "Table already full";
	for (int i = 0; i < this->DataCount; i++)
		if (this->pRecs[i]->GetKey() == k)
			throw "Key already exist";
	this->pRecs[this->DataCount] = new TTabRecord(k,val);
	this->DataCount++;
}

void TScanTable::DelRecord(TKey k)
{
	if (this->IsEmpty())
		throw "Error, table is empty";
	int i = 0;
	for (i = 0; i < this->DataCount; i++)
	{
		if (this->pRecs[i]->GetKey() == k)break;
	}
	if (i == this->DataCount) throw "Error key";

	this->pRecs[i] = this->pRecs[this->DataCount-1];
	delete this->pRecs[this->DataCount - 1];
	this->DataCount--;
}